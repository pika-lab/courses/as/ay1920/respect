# TuCSoN/ReSpecT exercises

### Important remarks

* You can start a TuCSoN **node** by running:

    ```bash
    ./gradlew tucson
    ```
  
    this will start a service listening on port 20504.
    
    * You can specify another port by starting the node as follows:
        
        ```bash
        ./gradlew tucson -Pport="port here"
        ```

* Always remember to run:

    ```bash
    ./gradlew --stop
    ```

    after each demo is completed, in order to ensure all TuCSoN nodes are actually shutdown.

* You can start a TuCSoN **inspector** by running:

    ```bash
    ./gradlew inspector
    ```
  
    A GUI application will start and you will be able to select the actual node and tuple centre to inspect

* You can always start a TuCSoN CLI (Command Line Interface) by running:

    ```bash
    ./gradlew cli -Pport=<node port>
    ```
  
    The `-Pport=<node port>` part is optional, and the `port` property defaults to `20504`.

## Example 0 -- TuCSoN Basics (`it.unibo.tucson.helloWorld` package)

0. Start a new TuCSoN node, a new inspector, on two different shells

    ```bash
    ./gradlew tucson
    ./gradlew inspector
    ```

0. On some third shell, run the `t.unibo.tucson.helloWorld.HelloWorld` class:

    ```bash
    ./gradlew helloWorld
    ```
   
   It consists of a class exploiting the TuCSoN API to interact with the aforementioned TuCSoN service
   
0. Usually the TuCSoN API is not used directly. It is aimed at letting TuCSoN agents interact over the network.
Consider for instance the `t.unibo.tucson.helloWorld.HelloWorldAgent` class, which extends `AbstractTucsonAgent`
    
    ```bash
    ./gradlew helloWorld
    ```

## Exercise 1 -- ReSpecT Basics (`it.unibo.respect.simple` package)

1. Start a new TuCSoN node, a new inspector, and a new CLI on three different shells

    ```bash
    ./gradlew tucson
    ./gradlew inspector
    ./gradlew cli
    ```

2. Use the inspector facilities to load the `it/unibo/respect/simple/aggregator.rsp` ReSpecT specification onto the 
`default` tuple centre
    * The simplest way is to just use copy & paste

3. Use che CLI to trigger reactions

4. Use the inspector to visualise the effects of reactions

5. Repeat the process with the `it/unibo/respect/simple/lazy_aggregators.rsp` ReSpecT specification

5. __\[Activity\]__ Modify the ReSpecT code as follows:
    - each reaction is logged by outing a tuple on another tuple centre, say `log@localhost:20504`

## Exercise 2 -- Bag of Tasks (`it.unibo.respect.bagOfTasks` package)

1. Have a look to the files in `it.unibo.respect.bagOfTasks.*`, there included the `bagoftask.rsp` ReSpecT specification
in `resources/`

2. Start a new TuCSoN node, and a new inspector on two different shells

    ```bash
    ./gradlew tucson
    ./gradlew inspector
    ```
   
3. Start the `BagOfTaskTest` program:

    ```bash
    ./gradlew bagOfTasks 
    ```
   
4. Use the inspector to visualise the interaction among the master and the workers

5. __\[Activity\]__ Modify the code as follows:
    - each agent periodically checks for the existence of a tuple in the form `die("agentId")`, where `agentId` is its own identifier: in case it is found, the agents gracefully terminates
    - the insertion of tuple `die_all` provokes the termination of all agents

## Exercise 3 -- Dining Philosophers (`it.unibo.respect.diningPhilosophers` package)

1. Have a look to the files in `it.unibo.respect.diningPhilosophers.*`, there included the `table.rsp` ReSpecT specification
in `resources/`

2. Start a new TuCSoN node, and a new inspector on two different shells

    ```bash
    ./gradlew tucson
    ./gradlew inspector
    ```
   
3. Start the `DiningPhilosophersTest` program:

    ```bash
    ./gradlew diningPhilosophers 
    ```
   
4. Use the inspector to visualise which chopsticks are on the table

5. __\[Activity\]__ Modify the code as follows:
    - if a philosopher is starving for a given amount of time (say 10 seconds), then it commits suicide
    - let a philosopher -- say n° 3 -- be greedy and keep eating over and over, without releasing its chops

## Exercise 4 -- **Distributed** Dining Philosophers (`it.unibo.respect.distributedDiningPhilosophers` package)

1. Have a look to the files in `it.unibo.respect.distributedDiningPhilosophers.*`, there included the `table.rsp` and `seat.rsp` ReSpecT specifications
in `resources/`

2. Start a new TuCSoN node, and a new inspector on two different shells

    ```bash
    ./gradlew tucson
    ./gradlew inspector
    ```
   
3. Start the `DDiningPhilosophersTest` program:

    ```bash
    ./gradlew distributedDiningPhilosophers 
    ```
   
4. Use the inspector to visualise which chopsticks are on the table, and which is the status of each seat
    + You may need more than one inspector

5. __\[Activity\]__ Modify the code as follows:
    - if a philosopher is starving for a given amount of time (say 10 seconds), then it commits suicide
    - let a philosopher -- say n° 3 -- be greedy and keep eating over and over, without releasing its chops
    
## Exercise 5 -- **Timed** Dining Philosophers (`it.unibo.respect.timedDiningPhilosophers` package)

1. Have a look to the files in `it.unibo.respect.distributedDiningPhilosophers.*`, there included the `table.rsp` ReSpecT specification
in `resources/`

2. Start a new TuCSoN node, and a new inspector on two different shells

    ```bash
    ./gradlew tucson
    ./gradlew inspector
    ```
   
3. Start the `TDiningPhilosophersTest` program:

    ```bash
    ./gradlew timedDiningPhilosophers 
    ```
   
4. Use the inspector to visualise which chopsticks are on the table

5. __\[Activity\]__ Modify the code as follows:
    - if a philosopher is starving for a given amount of time (say 10 seconds), then it commits suicide
    - let a philosopher -- say n° 3 -- be greedy and keep eating over and over, without releasing its chops
    - what's the difference in this case?
    
## Exercise 6 -- **Timed & Distributed** Dining Philosophers (`it.unibo.respect.tndDiningPhilosophers` package)

* Up to you :)