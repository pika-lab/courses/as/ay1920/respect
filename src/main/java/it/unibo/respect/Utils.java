/**
 * This work by Danilo Pianini is licensed under a Creative Commons
 * Attribution-NonCommercial-ShareAlike 3.0 Italy License.
 * Permissions beyond the scope of this license may be available at www.danilopianini.org.
 */
package it.unibo.respect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Credits go to the author below.
 *
 * @author Danilo Pianini
 */
public class Utils {

    public static String fileToString(URL resource) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                Objects.requireNonNull(resource).openStream()
        ))) {
            return br.lines().collect(Collectors.joining("\n"));
        }
    }

}
