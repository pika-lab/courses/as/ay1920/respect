package it.unibo.respect.timedDiningPhilosophers;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.TupleCentreIdentifier;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.AbstractTucsonAgent;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonOperation;
import alice.tuplecentre.tucson.api.acc.EnhancedACC;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

/**
 * A Dining Philosopher: thinks and eats in an endless loop.
 *
 * @author s.mariani@unibo.it
 */
public class TDiningPhilosopher extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static final Long TIMEOUT = null; // no timeout

    private final int chop1;
    private final int chop2;
    private final int time;
    private final int step;
    private final TupleCentreIdentifier myTable;

    public TDiningPhilosopher(String aid, TupleCentreIdentifier table,
                              int left, int right, int eatingTime,
                              int eatingStep) throws TucsonInvalidAgentIdException {
        super(aid);
        myTable = table;
        chop1 = left;
        chop2 = right;
        time = eatingTime;
        step = eatingStep;
    }

    private boolean acquireChops() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        TucsonOperation op = getACC().in(myTable, LogicTuple.parse("chops(" + chop1 + "," + chop2 + ")"), TIMEOUT);
        return op != null && op.isResultSuccess();
    }

    private boolean eat() throws InterruptedException, InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        say("...gnam gnam...chomp chomp...munch munch...");
        for (int i = 0; i < time / step; i++) {
            Thread.sleep(step);
            TucsonOperation op  = getACC().rdp(
                    myTable,
                    LogicTuple.parse("used(" + chop1 + "," + chop2 + ",_)"),
                    TIMEOUT
            );
            if (!op.isResultSuccess()) {
                return false;
            }
        }
        return true;
    }

    private void releaseChops() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        getACC().out(
                myTable,
                LogicTuple.parse("chops(" + chop1 + "," + chop2 + ")"),
                TIMEOUT
        );
    }

    private void think() throws InterruptedException {
        say("...mumble mumble...rat rat...mumble mumble...");
        Thread.sleep(5000);
    }

    @Override
    protected EnhancedACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        // Ugly but effective, pardon me...
        while (true) {
            say("Now thinking...");
            think();
            say("I'm hungry, let's try to eat something...");
            /*
             * Try to get needed chopsticks.
             */
            if (acquireChops()) {
                /*
                 * If successful eat.
                 */
                if (eat()) {
                    say("I'm done, wonderful meal :)");
                    /*
                     * Then release chops.
                     */
                    releaseChops();
                } else {
                    say("OMG my chopsticks disappeared!");
                }
            } else {
                say("I'm starving!");
            }
        }
    }

}
