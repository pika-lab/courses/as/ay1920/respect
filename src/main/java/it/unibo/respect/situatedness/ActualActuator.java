/**
 * java
 */
package it.unibo.respect.situatedness;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.respect.core.TransducersManager;
import alice.tuplecentre.respect.situatedness.*;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonOperation;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

/**
 * The 'actual' actuator probe deployed in this scenario. Although in this toy
 * example it is only simulated, here is where you would place your code to
 * interface with a real-world probe.
 *
 * @author ste (mailto: s.mariani@unibo.it) on 06/nov/2013
 *
 */
public class ActualActuator implements Probe {

    private static final String DEFAULT_HOST = "localhost";
    private static final String DEFAULT_PORT = "20504";
    private final EnhancedSyncACC acc;
    private final ProbeIdentifier pid;
    private final TucsonTupleCentreId tempTc;
    private TransducerId tid;
    private TransducerStandardInterface transducer;

    public ActualActuator(ProbeIdentifier i) {
        pid = i;
        try {
            TucsonAgentId aid = TucsonAgentId.of("actuator");
            acc = TucsonMetaACC.getContext(aid, DEFAULT_HOST, Integer.parseInt(DEFAULT_PORT));
            tempTc = TucsonTupleCentreId.of("tempTc", DEFAULT_HOST, DEFAULT_PORT);
        } catch (TucsonInvalidTupleCentreIdException | TucsonInvalidAgentIdException e) {
            throw new IllegalStateException(e);
        }
    }
    
    @Override
    public ProbeIdentifier getIdentifier() {
        return pid;
    }
    
    @Override
    public TransducerId getTransducer() {
        return tid;
    }
    
    @Override
    public void setTransducer(TransducerId t) {
        tid = t;
    }
    
    @Override
    public boolean readValue(String key) {
        System.err.println("[" + pid + "]: I'm an actuator, I can't sense values!");
        return false;
    }
    
    @Override
    public boolean writeValue(String key, int value) {
        if (!"temp".equals(key)) {
            System.err.println("[" + pid + "]: Unknown property " + key);
            return false;
        }
        if (tid == null) {
            System.err.println("[" + pid + "]: Don't have any transducer associated yet!");
            return false;
        }
        if (transducer == null) {
            transducer = TransducersManager.INSTANCE.getTransducer(tid.getLocalName());
            if (transducer == null) {
                System.err.println("[" + pid + "]: Can't retrieve my transducer!");
                return false;
            }
        }
        try {
            LogicTuple template = LogicTuple.parse("temp(_)");
            TucsonOperation op = acc.inAll(tempTc, template, null);
            if (op.isResultSuccess()) {
                LogicTuple tempTuple = LogicTuple.parse("temp(" + value + ")");
                acc.out(tempTc, tempTuple, null);
                System.out.println("[" + pid + "]: temp set to " + value);
                transducer.notifyEnvEvent(key, value, AbstractTransducer.SET_MODE);
                return true;
            }
        } catch (TucsonOperationNotPossibleException | UnreachableNodeException
                | OperationTimeOutException | InvalidLogicTupleException e) {
            throw new IllegalStateException(e);
        }
        return false;
    }
}
