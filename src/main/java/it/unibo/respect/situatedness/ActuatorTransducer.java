package it.unibo.respect.situatedness;

import alice.tuplecentre.api.TupleCentreIdentifier;
import alice.tuplecentre.core.AbstractTupleCentreOperation;
import alice.tuplecentre.respect.situatedness.AbstractTransducer;
import alice.tuplecentre.respect.situatedness.Probe;
import alice.tuplecentre.respect.situatedness.TransducerId;
import alice.tuplecentre.tucson.api.TucsonOperation;

/**
 * The transducer mediating interactions to/from the actuator probe. As such,
 * only the 'setEnv' method is implemented (furthermore, a synchronous behaviour
 * is expected, hence no asynchronous facility is implemented).
 *
 * @author ste (mailto: s.mariani@unibo.it) on 05/nov/2013
 *
 */
public class ActuatorTransducer extends AbstractTransducer {

    /**
     * @param i
     *            the transducer id
     * @param tc
     *            the tuple centre id
     */
    public ActuatorTransducer(final TransducerId i, final TupleCentreIdentifier tc) {
        super(i, tc);
    }

    @Override
    public boolean getEnv(final String key) {
        speakErr("[" + id + "]: I'm an actuator transducer, I can't sense values!");
        return false;
    }


    /*
     * (non-Javadoc)
     * @see
     * alice.respect.situatedness.AbstractTransducer#setEnv(java.lang.String,
     * int)
     */
    @Override
    public boolean setEnv(final String key, final int value) {
        speak("[" + id + "]: Writing...");
        /*
         * for each probe this transducer models, stimulate it to act on its
         * environment
         */
        return probes.values().stream()
                .map(Probe.class::cast)
                .map(probe -> probe.writeValue(key, value))
                .peek(result -> {
                    if (!result) {
                        speakErr("[" + id + "]: Write failure!");
                    }
                })
                .allMatch(result -> result);
    }

    @Override
    public void operationCompleted(TucsonOperation tucsonOperation) {
        /* not used */
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation op) {
        /* not used */
    }
}
