package it.unibo.respect.situatedness;

import alice.tuplecentre.api.TupleCentreIdentifier;
import alice.tuplecentre.core.AbstractTupleCentreOperation;
import alice.tuplecentre.respect.situatedness.AbstractTransducer;
import alice.tuplecentre.respect.situatedness.Probe;
import alice.tuplecentre.respect.situatedness.TransducerId;
import alice.tuplecentre.tucson.api.TucsonOperation;

/**
 * The transducer mediating interactions to/from the sensor probe. As such, only
 * the 'getEnv' method is implemented (furthermore, a synchronous behaviour is
 * expected, hence no asynchronous facility is implemented).
 *
 * @author ste (mailto: s.mariani@unibo.it) on 05/nov/2013
 *
 */
public class SensorTransducer extends AbstractTransducer {

    /**
     * @param i
     *            the transducer identifier
     * @param tc
     *            the tuple centre identifier
     */
    public SensorTransducer(final TransducerId i, final TupleCentreIdentifier tc) {
        super(i, tc);
    }

    @Override
    public boolean getEnv(final String key) {
        speak("[" + id + "]: Reading...");
        /*
         * for each probe this transducer models, stimulate it to act on its
         * environment
         */
        return probes.values().stream()
                .map(Probe.class::cast)
                .map(probe -> probe.readValue(key))
                .peek(result -> {
                    if (!result) {
                        speakErr("[" + id + "]: Read failure!");
                    }
                })
                .allMatch(result -> result);
    }

    @Override
    public boolean setEnv(final String key, final int value) {
        speakErr("[" + id + "]: I'm a sensor transducer, I can't set values!");
        return false;
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation op) {
        /* not used */
    }

    @Override
    public void operationCompleted(final TucsonOperation op) {
        /* not used */
    }
}
