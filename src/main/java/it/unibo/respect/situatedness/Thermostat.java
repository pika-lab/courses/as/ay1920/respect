/**
 * java
 */
package it.unibo.respect.situatedness;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonOperation;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import it.unibo.respect.Utils;

/**
 * TuCSoN situatedness feature example.
 * <p>
 * In this toy scenario, a situated, 'intelligent' thermostat is in charge of
 * keeping a room temperature between 18 and 22. In order to do so, it is
 * equipped with a sensor (ActualSensor class) and an actuator (ActualActuator
 * class). As obvious, the former is requested by the thermostat to perceiving
 * the temperature, whereas the latter is prompted to change the temperature
 * upon need.
 * <p>
 * Whereas the thermostat entity can be programmed as pleased, hence as an agent
 * or a simple Java process (still a TuCSoN agent, as in this case), the sensor
 * and the actuator should be modelled as "probes" (aka environmental
 * resources), interfacing with the MAS (in this simple case, only the
 * thermostat TuCSoN agent) through one transducer each.
 * <p>
 * Furthermore, to leverage a possible ditributed scenario for this toy
 * thermostat example, transducers and the thermostat each have their own tuple
 * centre to interact with, suitably programmed through situated ReSpecT
 * reactions (sensorSpec.rsp and actuatorSpec.rsp).
 *
 * @author ste (mailto: s.mariani@unibo.it) on 05/nov/2013
 */
public class Thermostat {

    private static final String DEFAULT_HOST = "localhost";
    private static final String DEFAULT_PORT = "20504";
    private static final int HIGH = 22;
    private static final int ITERS = 10;
    private static final int LOW = 18;

    private Thermostat() { }

    /**
     * @param args no args expected
     */
    public static void main(String[] args) throws Exception {
        TucsonAgentId aid = TucsonAgentId.of("thermostat");
        NegotiationACC negACC = TucsonMetaACC.getNegotiationContext(aid, DEFAULT_HOST, Integer.parseInt(DEFAULT_PORT));
        EnhancedSyncACC acc = negACC.playDefaultRole();
        /*
         * EnhancedSynchACC acc = TucsonMetaACC.getContext(aid,
         * DEFAULT_HOST,
         * Integer.valueOf(DEFAULT_PORT));
         */
        TucsonTupleCentreId configTc = TucsonTupleCentreId.of("'$ENV'", DEFAULT_HOST, DEFAULT_PORT);
        /* Set up temperature */
        TucsonTupleCentreId tempTc = TucsonTupleCentreId.of("tempTc", DEFAULT_HOST, DEFAULT_PORT);
        int bootT;
        do {
            // 10 < bootT < LOW || HIGH < bootT < 30
            bootT = Math.round((float) (Math.random() * 20)) + 10;
        } while (bootT >= LOW && bootT <= HIGH);
        LogicTuple bootTemp = LogicTuple.parse("temp(" + bootT + ")");
        acc.out(tempTc, bootTemp, null);
        /* Set up sensor */
        log(aid.toString(), "Set up sensor...");
        TucsonTupleCentreId sensorTc = TucsonTupleCentreId.of("sensorTc", DEFAULT_HOST, DEFAULT_PORT);
        acc.setS(sensorTc,
                Utils.fileToString(Thermostat.class.getResource("sensorSpec.rsp")),
                null);
        LogicTuple sensorTuple = LogicTuple.of(
                "createTransducerSensor",
                TupleArgument.fromTerm(sensorTc.toTerm()),
                TupleArgument.of(SensorTransducer.class.getName()),
                TupleArgument.of("sensorTransducer"),
                TupleArgument.of(ActualSensor.class.getName()),
                TupleArgument.of("sensor"));
        acc.out(configTc, sensorTuple, null);
        /* Set up actuator */
        log(aid.toString(), "Set up actuator...");
        TucsonTupleCentreId actuatorTc = TucsonTupleCentreId.of("actuatorTc", DEFAULT_HOST, DEFAULT_PORT);
        acc.setS(actuatorTc,
                Utils.fileToString(Thermostat.class.getResource("actuatorSpec.rsp")),
                null);
        LogicTuple actuatorTuple = LogicTuple.of(
                "createTransducerActuator",
                TupleArgument.fromTerm(actuatorTc.toTerm()),
                TupleArgument.of(ActuatorTransducer.class.getName()),
                TupleArgument.of("actuatorTransducer"),
                TupleArgument.of(ActualActuator.class.getName()),
                TupleArgument.of("actuator"));
        acc.out(configTc, actuatorTuple, null);
        /* Start perception-reason-action loop */
        log(aid.toString(), "Start perception-reason-action loop...");
        LogicTuple template;
        TucsonOperation op;
        int temp;
        LogicTuple action = null;
        for (int i = 0; i < ITERS; i++) {
            Thread.sleep(3000);
            /* Perception */
            template = LogicTuple.parse("sense(temp(_))");
            op = acc.in(sensorTc, template, null);
            if (op.isResultSuccess()) {
                temp = op.getLogicTupleResult().getArg(0).getArg(0).intValue();
                log(aid.toString(), "temp is " + temp + " hence...");
                /* Reason */
                if (temp >= LOW && temp <= HIGH) {
                    log(aid.toString(), "...nothing to do");
                    continue;
                } else if (temp < LOW) {
                    log(aid.toString(), "...heating up");
                    action = LogicTuple.parse("act(temp(" + ++temp + "))");
                } else if (temp > HIGH) {
                    log(aid.toString(), "...cooling down");
                    action = LogicTuple.parse("act(temp(" + --temp + "))");
                }
                /* Action */
                acc.out(actuatorTc, action, null);
            }
        }
    }

    private static void log(String who, String msg) {
        System.out.println("[" + who + "]: " + msg);
    }
}
