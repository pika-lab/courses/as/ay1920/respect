package it.unibo.respect.bagOfTask;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

import java.util.Random;

/**
 * Master thread of a bag-of-task architecture. Given a TuCSoN Node (optional)
 * 1) it programs the specification space so as to perform an average
 * computation; 2) it submits jobs at random regarding summation/subtraction
 * computation (to be carried out by workers); 3) then collects results of such
 * tasks and the average (computed by the tuplecentre itself thanks to ReSpecT
 * reactions).
 *
 * @author s.mariani@unibo.it
 */
public class Master extends AbstractTucsonAgent<EnhancedSyncACC> {

    private static final int ITERs = 10;
    private static final Long TIMEOUT = null; // no timeout
    
    private final TucsonTupleCentreId tid;

    /*
     * To randomly choose between summation and subtraction.
     */
    private final Random r = new Random();

    public Master(String aid, String tid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        this.tid = TucsonTupleCentreId.of(tid, "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    private int randInt(int maxEx) {
        return r.nextInt(maxEx);
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC acc = getACC();
        LogicTuple task = null;

        /*
         * Start tasks submission cycle...
         */
        for (int i = 0; i < ITERs; i++) {
            if (r.nextBoolean()) {
                task = LogicTuple.parse("task(sum(" + randInt(ITERs) + "," + randInt(ITERs) + "))");
            } else {
                task = LogicTuple.parse("task(sub(" + randInt(ITERs) + "," + randInt(ITERs) + "))");
            }
            say("Injecting task: " + task + "...");
            acc.out(tid, task, TIMEOUT);
            // Thread.sleep(1000);
        }

        /*
         * ...then wait the result to be computed by ReSpecT reaction chaining.
         */
        LogicTuple resTempl = LogicTuple.parse("result(Res," + ITERs + ")");
        say("Waiting for result...");
        TucsonOperation resOp = acc.in(tid, resTempl, TIMEOUT);
        LogicTuple res = resOp.getLogicTupleResult();
        say("Result is: " + res.getArg(0));
        say("Average is: " + res.getArg(0).floatValue() / res.getArg(1).floatValue());
    }

}
