package it.unibo.respect.bagOfTask;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

/**
 * Worker thread of a bag-of-task architecture. Given a TuCSoN Node (optional)
 * 1) it waits for jobs exploiting TuCSoN primitives timeout to proper terminate
 * (or to react to failures...) 2) it performs the correct computation
 * (summation/subtraction solely, not the average!) 3) then puts back in the
 * space the result.
 *
 * @author s.mariani@unibo.it
 */
public class Worker extends AbstractTucsonAgent<EnhancedSyncACC> {

    private static final long TIMEOUT = 10_000; // 10 s
    private final TucsonTupleCentreId tid;

    public Worker(String aid, String tid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        this.tid = TucsonTupleCentreId.of(tid, "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    private int sub(TupleArgument arg, TupleArgument arg2) {
        say("sub(" + arg.intValue() + "," + arg2.intValue() + ")...");
        /*
         * "Smart" subtraction.
         */
        if (arg.intValue() > arg2.intValue()) {
            return arg.intValue() - arg2.intValue();
        }
        return arg2.intValue() - arg.intValue();
    }

    private int sum(TupleArgument arg, TupleArgument arg2) {
        say("sum(" + arg.intValue() + "," + arg2.intValue() + ")...");
        return arg.intValue() + arg2.intValue();
    }

    @Override
    protected void main() throws Exception {
        try {
            EnhancedSyncACC acc = getACC();

            while (true) {
                LogicTuple taskTemplate = LogicTuple.parse("task(OP)");
                say("Waiting for task...");

                /*
                 * Usage of timeouts: be careful that timeout extinction DOES
                 * NOT IMPLY operation removal from TuCSoN Node!
                 */
                TucsonOperation taskOp = acc.in(tid, taskTemplate, TIMEOUT);
                LogicTuple task = taskOp.getLogicTupleResult();

                /*
                 * Perform the correct computation.
                 */
                int aggregated;
                if (task.getArg(0).getName().equals("sum")) {
                    aggregated = sum(task.getArg("sum").getArg(0), task.getArg("sum").getArg(1));
                } else {
                    aggregated = sub(task.getArg("sub").getArg(0), task.getArg("sub").getArg(1));
                }

                /*
                 * Put back result.
                 */
                LogicTuple res = LogicTuple.parse("res(" + aggregated + ")");
                say("Injecting result: " + res + "...");
                acc.out(tid, res, TIMEOUT);
                // Thread.sleep(1000);
            }
        } catch (OperationTimeOutException e) {
            say("Timeout exceeded, I quit");
        }
    }

}
