package it.unibo.respect.bagOfTask;

import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import it.unibo.respect.Utils;

/**
 * NOT configured to run multiple masters. How to do so? Does it run
 * INDEPENDENTLY of master/workers launch ORDER?
 *
 * @author s.mariani@unibo.it
 */
public class BagOfTaskTest {

    private static final String TC_NAME = "bagoftask";
    private static final Long TIMEOUT = null; // no timeout

    /**
     * @param args no args expected.
     */
    public static void main(final String[] args) throws Exception {
        configureReactions("bagoftask.rsp");

        new Master("master", TC_NAME).go();
        new Worker("worker1", TC_NAME).go();
        new Worker("worker2", TC_NAME).go();
    }

    private static void configureReactions(String resourceName) throws Exception {
        EnhancedSyncACC acc = TucsonMetaACC.getNegotiationContext("configurator").playDefaultRole();
        acc.setS(
                TucsonTupleCentreId.of(TC_NAME, "localhost", "20504"),
                Utils.fileToString(BagOfTaskTest.class.getResource(resourceName)),
                TIMEOUT
        );
    }

}
