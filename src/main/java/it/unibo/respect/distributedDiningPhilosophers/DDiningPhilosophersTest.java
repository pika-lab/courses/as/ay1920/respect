package it.unibo.respect.distributedDiningPhilosophers;


import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.api.TupleCentreIdentifier;
import alice.tuplecentre.tucson.api.AbstractTucsonAgent;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedACC;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import it.unibo.respect.Utils;

/**
 * @author ste (mailto: s.mariani@unibo.it)
 */
public class DDiningPhilosophersTest extends AbstractTucsonAgent<EnhancedACC> {

    public static final Long TIMEOUT = null; // no timeout

    private static final String PORT = "20504";
    private static final String HOST = "localhost";

    private static final int N_PHILOSOPHERS = 5;

    /**
     * @param aid the String representation of a valid TuCSoN agent identifier
     * @throws TucsonInvalidAgentIdException if the given String does not represent a valid TuCSoN agent
     *                                       identifier
     */
    public DDiningPhilosophersTest(String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    /**
     * @param args no args expected
     */
    public static void main(String[] args) throws TucsonInvalidAgentIdException {
        new DDiningPhilosophersTest("boot").go();
    }

    @Override
    protected EnhancedACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC acc = getACC();
        TupleCentreIdentifier[] seats = new TupleCentreIdentifier[N_PHILOSOPHERS];
        for (int i = 0; i < N_PHILOSOPHERS; i++) {
            seats[i] = TucsonTupleCentreId.of("seat(" + i + "," + (i + 1) % N_PHILOSOPHERS + ")", HOST, PORT);
            say("Injecting 'seat' ReSpecT specification in tc < " + seats[i].toString() + " >...");
            acc.setS(
                    seats[i],
                     Utils.fileToString(this.getClass().getResource("seat.rsp")),
                     TIMEOUT
            );
            acc.out(seats[i], LogicTuple.parse("philosopher(thinking)"), null);
        }
        TucsonTupleCentreId table = TucsonTupleCentreId.of("table", HOST, PORT);
        say("Injecting 'table' ReSpecT specification in tc < " + table.toString() + " >...");
        acc.setS(
                table,
                Utils.fileToString(this.getClass().getResource("table.rsp")),
                TIMEOUT
        );
        for (int i = 0; i < N_PHILOSOPHERS; i++) {
            acc.out(table, LogicTuple.parse("chop(" + i + ")"), TIMEOUT);
        }
        for (int i = 0; i < N_PHILOSOPHERS; i++) {
            new DDiningPhilosopher("'philo-" + i + "'", seats[i]).go();
        }
    }
}
