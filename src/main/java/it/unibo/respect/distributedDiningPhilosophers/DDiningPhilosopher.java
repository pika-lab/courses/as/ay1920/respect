package it.unibo.respect.distributedDiningPhilosophers;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.api.TupleCentreIdentifier;
import alice.tuplecentre.tucson.api.AbstractTucsonAgent;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonOperation;
import alice.tuplecentre.tucson.api.acc.EnhancedACC;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;

/**
 *
 * @author ste (mailto: s.mariani@unibo.it)
 *
 */
public class DDiningPhilosopher extends AbstractTucsonAgent<EnhancedACC> {

    public static final Long TIMEOUT = null; // no timeout

    private static final int EATING_TIME = 5000;
    private static final int THINKING_TIME = 5000;
    private final TupleCentreIdentifier mySeat;

    /**
     *
     * @param aid
     *            the String representation of this philosopher's TuCSoN agent
     *            identifier
     * @param seat
     *            the identifier of the TuCSoN tuple centre representing the
     *            philosopher's seat
     * @throws TucsonInvalidAgentIdException
     *             if the given String does not represent a valid TuCSoN agent
     *             identifier
     */
    public DDiningPhilosopher(final String aid, final TupleCentreIdentifier seat) throws TucsonInvalidAgentIdException {
        super(aid);
        mySeat = seat;
    }

    @Override
    protected EnhancedACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(getTucsonAgentId()).playDefaultRole();
    }

    private void eating() throws InterruptedException {
        say("...gnam gnam...chomp chomp...munch munch...");
        Thread.sleep(EATING_TIME);
    }

    private void think() throws InterruptedException {
        say("...mumble mumble...rat rat...mumble mumble...");
        Thread.sleep(THINKING_TIME);
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC acc = getACC();
        TucsonOperation op;
        // Ugly but effective, pardon me...
        while (true) {
            say("Now thinking...");
            think();

            say("I'm hungry, let's try to eat something...");

            op = acc.in(mySeat, LogicTuple.parse("wanna_eat"), TIMEOUT);
            if (op.isResultSuccess()) {
                eating();
                say("I'm done, wonderful meal :)");
                acc.in(mySeat, LogicTuple.parse("wanna_think"), TIMEOUT);
            } else {
                say("I'm starving!");
            }
        }
    }
}
