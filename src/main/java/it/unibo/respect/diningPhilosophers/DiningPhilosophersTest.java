package it.unibo.respect.diningPhilosophers;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.AbstractTucsonAgent;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import it.unibo.respect.Utils;

/**
 * Classic Dining Philosophers coordination problem tackled by adopting a clear
 * separation of concerns between coordinables (philosophers) and coordination
 * medium (table) thanks to TuCSoN ReSpecT tuple centres programmability.
 *
 * @author s.mariani@unibo.it
 */
public class DiningPhilosophersTest extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static final Long TIMEOUT = null; // no timeout

    /*
     * Max number of simultaneously eating philosophers should be N_PHILOSOPHERS-2.
     */
    private static final int N_PHILOSOPHERS = 5;
    private final String ip;
    private final String port;

    public DiningPhilosophersTest(String aid) throws TucsonInvalidAgentIdException {
        super(aid);
        /*
         * To experiment with a distributed setting, launch the TuCSoN Node
         * hosting the 'table' tuple centre on a remote node.
         */
        ip = "localhost";
        port = "20504";
    }

    /**
     *
     * @param args no args expected
     */
    public static void main(String[] args) throws TucsonInvalidAgentIdException {
        new DiningPhilosophersTest("boot").go();
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(this.getTucsonAgentId()).playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        EnhancedSyncACC acc = getACC();
        TucsonTupleCentreId table = TucsonTupleCentreId.of("table", ip, port);
        say("Injecting 'table' ReSpecT specification in tc < " + table.toString() + " >...");
        /*
         * Program the tuple centre by setting a ReSpecT specification (a
         * set of ReSpecT specification tuples) in its specification space.
         */
        acc.setS(
                table,
                Utils.fileToString(getClass().getResource("table.rsp")),
                TIMEOUT
        );

        for (int i = 0; i < N_PHILOSOPHERS; i++) {
            /*
             * Init chopsticks required to eat.
             */
            acc.out(table, LogicTuple.parse("chop(" + i + ")"), TIMEOUT);
        }

        for (int i = 0; i < N_PHILOSOPHERS; i++) {
            /*
             * Start philosophers by telling them which chopsticks pair they need.
             */
            new DiningPhilosopher("'philo-" + i + "'", table, i, (i + 1) % N_PHILOSOPHERS).go();
        }
    }

}
