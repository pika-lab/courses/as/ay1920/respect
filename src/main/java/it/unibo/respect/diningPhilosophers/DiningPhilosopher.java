package it.unibo.respect.diningPhilosophers;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

/**
 * A Dining Philosopher: thinks and eats in an endless loop.
 *
 * @author s.mariani@unibo.it
 */
public class DiningPhilosopher extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static final Long TIMEOUT = null; // no timeout

    private final TucsonTupleCentreId myTable;

    private final int chop1;
    private final int chop2;

    public DiningPhilosopher(String aid, TucsonTupleCentreId table, int left, int right) throws TucsonInvalidAgentIdException {
        super(aid);
        myTable = table;
        chop1 = left;
        chop2 = right;
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        return TucsonMetaACC.getNegotiationContext(this.getTucsonAgentId()).playDefaultRole();
    }

    private boolean acquireChops() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        /*
         * NB: The 2 needed chopsticks are "perceived" as a single item by
         * the philosophers, while the coordination medium correctly handle
         * them separately.
         */
        TucsonOperation op = getACC().in(
                myTable,
                LogicTuple.parse("chops(" + chop1 + "," + chop2 + ")"),
                TIMEOUT
        );
        return op != null && op.isResultSuccess();
    }

    private void eat() throws InterruptedException {
        say("...gnam gnam...chomp chomp...munch munch...");
        Thread.sleep(5000);
    }

    private void releaseChops() throws InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        getACC().out(
                myTable,
                LogicTuple.parse("chops(" + chop1 + "," + chop2 + ")"),
                TIMEOUT
        );
    }

    private void think() throws InterruptedException {
        say("...mumble mumble...rat rat...mumble mumble...");
        Thread.sleep(5000);
    }

    @Override
    protected void main() throws Exception {
        while (true) {
            say("Now thinking...");
            think();
            say("I'm hungry, let's try to eat something...");
            /*
             * Try to get needed chopsticks.
             */
            if (acquireChops()) {
                /*
                 * If successful eat.
                 */
                eat();
                say("I'm done, wonderful meal :)");
                /*
                 * Then release chops.
                 */
                releaseChops();
            } else {
                say("I'm starving!");
            }
        }
    }

}
